# README

## Add Nuget

```xml
<Project Sdk="">
  <ItemGroup>
    <PackageReference Include="TestNuget.FFmpeg" Version="1.0.0" GeneratePathProperty="true">
      <IncludeAssets>none</IncludeAssets>
    </PackageReference>
  </ItemGroup>

  <Target Name="Copyffmpeg" AfterTargets="Build" Condition="'$(TargetFramework)' == 'net472'">
    <Copy SourceFiles="$(PkgTestNuget_FFmpeg)\runtimes\win-x64\native\ffmpeg.exe" DestinationFolder="$(OutDir)\runtimes\win-x64\native\" />
    <Copy SourceFiles="$(PkgTestNuget_FFmpeg)\runtimes\win-x86\native\ffmpeg.exe" DestinationFolder="$(OutDir)\runtimes\win-x86\native\" />
  </Target>

</Project>
```